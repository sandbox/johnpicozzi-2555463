Developer Notes
=======================

https://www.drupal.org/sandbox/johnpicozzi/2555463

INTRODUCTION
------------

The Developer notes module will allow a site Admin to create and manage
"Pass Along" type notes to others with the correct permissions. This will
create a content type and a block that will be used to add and display notes
for admin users.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/johnpicozzi/2555463

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2555463

REQUIREMENTS
------------
This module requires the following modules:

 * Views (https://drupal.org/project/views)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7 for
  further information.

CONFIGURATION
-------------

ROADMAP
-------------

-Purpose-
To allow admins to pass along important "notes" to one another. 

-Use Case-
* .htaccess updates
* patches to modules
* hacks to code
* General Gotchas

-Build + Functionality-
* Content Type for Notes (Started - Needs Work)
* Permissions to Access/ Add / Delete Notes (started - Needs Work)
* Block to display notes on Admin Login
  * Add to user Screen / Admin option to show hide info
  * Block Allows for Sticky at top notes
  * Block Shows Last 10 Notes
    * Title + Link
    * link to view all
  * View for all Notes (Started - Need Work / See views export file)